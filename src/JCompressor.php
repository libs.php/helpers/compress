<?php
/**
 * JCompressor.php
 * /src
 * 
 * @package JCompressor
 * @author Jesús Rojas <jrojash@jys.pe>
 * @copyright 2023, J&S Perú <https://jys.pe>
 * @created 2023-09-05 15:28:49
 * @version 20230905165711 (Rev. 2209)
 * @filesource
 */

use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use MatthiasMullie\Minify;

class JCompressor
{
    /**
     * Errores que se pueden haber generado durante el comprimido del contenido CSS o JS
     * @var array
     */
    public static $errors = [];

    /**
     * Compimir HTML de forma sencilla
     * @param string $buffer
     * @return string
     */
    public static function html(string $buffer): string
    {
        $replaces = [
            ## strip whitespaces after tags, except space
            '/\>[^\S ]+/s'      => '>',

            ## strip whitespaces before tags, except space
            '/[^\S ]+\</s'      => '<',

            ## shorten multiple whitespace sequences
            '/(\s)+/s'          => '\\1',

            ## Remove HTML comments
            '/<!--(.|\s)*?-->/' => '',
        ];

        $buffer = preg_replace(array_keys($replaces), array_values($replaces), $buffer);
        return $buffer;
    }

    /**
     * Comprimir contenido CSS
     * @param string $content
     * @param bool $use_toptal
     * @param int|null $cache_time
     * @param string|null $cache_key
     * @return string
     */
    public static function css(string $content, bool $use_toptal = false, int|null $cache_time = null, string|null $cache_key = null): string
    {
        if (empty($content))
            return '';

        if (!is_null($cache_time)) {
            $cache_key = $cache_key ?? md5($content);
            $cache = new FilesystemAdapter();
            $cache_item = $cache->getItem($cache_key);

            if ($cache_item->isHit())
                return $cache_item->get();
        }

        $minifier = new Minify\CSS;
        $minifier->add($content);

        try {
            $temp = $minifier->minify();
            $content = $temp;
        }
        catch (Throwable $ex) {
            ## something fail
            return $content;
        }

        if ($use_toptal) {
            $temp = static::toptal('https://www.toptal.com/developers/cssminifier/api/raw', $content);
            is_null($temp) or $content = $temp;
        }

        if (isset($cache_item)) {
            $cache_item->set($content);
            $cache_item->expiresAfter($cache_time);
            $cache->save($cache_item);
        }

        return $content;
    }

    /**
     * Comprimir contenido Javascript
     * @param string $content
     * @param bool $use_toptal
     * @param int|null $cache_time
     * @param string|null $cache_key
     * @return string
     */
    public static function js(string $content, bool $use_toptal = false, int|null $cache_time = null, string|null $cache_key = null): string
    {
        if (empty($content))
            return '';

        if (!is_null($cache_time)) {
            $cache_key = $cache_key ?? md5($content);
            $cache = new FilesystemAdapter();
            $cache_item = $cache->getItem($cache_key);

            if ($cache_item->isHit())
                return $cache_item->get();
        }

        $minifier = new Minify\JS;
        $minifier->add($content);

        try {
            $temp = $minifier->minify();
            $content = $temp;
        }
        catch (Throwable $ex) {
            ## something fail
            return $content;
        }

        if ($use_toptal) {
            $temp = static::toptal('https://www.toptal.com/developers/javascript-minifier/api/raw', $content);
            is_null($temp) or $content = $temp;
        }

        if (isset($cache_item)) {
            $cache_item->set($content);
            $cache_item->expiresAfter($cache_time);
            $cache->save($cache_item);
        }

        return $content;
    }

    /**
     * Comprimir el contenido JSON
     * @param string|array $content
     * @return string
     */
    public static function json(string|array $content): string
    {
        is_array($content) and $content = json_encode($content);

        if (empty($content))
            return '';

        $temp = json_decode($content, true);
        if (is_null($temp))
            return $content;

        $content = json_encode($temp);
        return $content;
    }

    /**
     * Comprimir el contenido de un archivo CSS
     * @param string $file
     * @param bool $use_toptal
     * @param int|null $cache_time
     * @param string|null $cache_key
     * @return string
     */
    public static function cssFile(string $file, bool $use_toptal = false, int|null $cache_time = null, string|null $cache_key = null): string
    {
        if (!file_exists($file))
            return '';

        $content = file_get_contents($file);

        return static::css($content, $use_toptal, $cache_time, $cache_key);
    }

    /**
     * Comprimir el contenido de un archivo Javascript
     * @param string $file
     * @param bool $use_toptal
     * @param int|null $cache_time
     * @param string|null $cache_key
     * @return string
     */
    public static function jsFile(string $file, bool $use_toptal = false, int|null $cache_time = null, string|null $cache_key = null): string
    {
        if (!file_exists($file))
            return '';

        $content = file_get_contents($file);

        return static::js($content, $use_toptal, $cache_time, $cache_key);
    }

    /**
     * Enviar un requests al portal toptal
     * 
     * URIs permitidos:
     * - https://www.toptal.com/developers/javascript-minifier/api/raw
     * - https://www.toptal.com/developers/cssminifier/api/raw
     * 
     * Posibles errores:
     * - 400 Missing input
     * - 405 HTTP Method not allowed - only POST is accepted
     * - 406 Content Type is not acceptable - only application/x-www-form-urlencoded is accepted.
     * - 413 Too large payload - max-size is 5MB.
     * - 422 Malformed input - for invalid javascript.
     * - 429 Too many requests - currently there is a limit of 30 requests per minute.
     * 
     * @param string $uri
     * @param string $input
     * @return bool|string
     */
    protected static function toptal(string $uri, string $input): string|null
    {
        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_URL            => $uri,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST           => true,
            CURLOPT_HTTPHEADER     => [
                'Content-Type: application/x-www-form-urlencoded'
            ],
            CURLOPT_POSTFIELDS     => http_build_query([
                'input' => $input
            ])
        ]);

        $minified = curl_exec($ch);

        if ($errno = curl_errno($ch)) {
            $error = curl_error($ch);
            static::$errors[] = [
                'microtime' => microtime(true),
                'time'      => time(),
                'code'      => $errno,
                'error'     => $error,
            ];
            curl_close($ch);

            return null;
        }

        if ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE) and $http_code <> 200) {
            curl_close($ch);

            $json = json_decode($minified, true);
            $errno = $http_code;

            if (is_null($json)) {
                $error = 'HTTP_CODE ' . $http_code;
                static::$errors[] = [
                    'microtime' => microtime(true),
                    'time'      => time(),
                    'code'      => $errno,
                    'error'     => $error,
                ];
                return null;
            }

            $errors = $json['errors'] ?? [[]];

            foreach ($errors as $error) {
                static::$errors[] = [
                    'microtime' => microtime(true),
                    'time'      => time(),
                    'code'      => $error['status'] ?? $errno,
                    'error'     => $error['title'] ?? 'Error desconocido',
                    'detail'    => $error['detail'] ?? null,
                ];
            }

            return null;
        }

        curl_close($ch);

        return $minified;
    }
}