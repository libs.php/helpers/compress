# Funciones Comprimir

## Instalación via composer

```bin
composer require jysperu/helpers-compress
```

## Funciones estáticas para comprimir contenidos Html/CSS/JS/JSON

```php
## Comprimir un contenido HTML
JCompressor::html(string $buffer): string;

## Comprimir un contenido CSS
JCompressor::css(string $content, bool $use_toptal = false, int|null $cache_time = null, string|null $cache_key = null): string;

## Comprimir un contenido JS
JCompressor::js(string $content, bool $use_toptal = false, int|null $cache_time = null, string|null $cache_key = null): string;

## Comprimir un contenido JSON
JCompressor::json(string|array $content): string;
```

## Uso simple

**Ejemplo 1:**

Minificar contenido CSS de forma local y rápida

```php
require_once 'vendor/autoload.php';

$css = '
p {
    color : red;
}
';

$minified = JCompressor::css($css); // p{color:red}
```

**Ejemplo 2:**

Minificar contenido JS de forma simple y sin cache

```php
require_once 'vendor/autoload.php';

$js = '
(function (factory) {
    if (typeof define === "function" && define.amd) {
        // AMD. Register as an anonymous module.
        define(["jquery"], factory);
    } else if (typeof module === "object" && module.exports) {
        // Node/CommonJS
        module.exports = function (root, jQuery) {
            if (jQuery === undefined) {
                // require("jQuery") returns a factory that requires window to
                // build a jQuery instance, we normalize how we use modules
                // that require this pattern but the window provided is a noop
                // if it"s defined (how jquery works)
                if (typeof window !== "undefined") {
                    jQuery = require("jquery");
                } else {
                    jQuery = require("jquery")(root);
                }
            }
            factory(jQuery);
            return jQuery;
        };
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

    $.fn.api = function () {
        return "";
    };

}));
';

$minified = JCompressor::js($js); 

/* // Resultado:
(function(factory){if(typeof define==="function"&&define.amd){define(["jquery"],factory)}else if(typeof module==="object"&&module.exports){module.exports=function(root,jQuery){if(jQuery===undefined){if(typeof window!=="undefined"){jQuery=require("jquery")}else{jQuery=require("jquery")(root)}}
factory(jQuery);return jQuery}}else{factory(jQuery)}}(function($){$.fn.api=function(){return""}}))
*/
```

**Ejemplo 3:**

Minificar contenido JS de forma mas completa utilizando el portal Toptal

```php
require_once 'vendor/autoload.php';

$js = '
(function (factory) {
    if (typeof define === "function" && define.amd) {
        // AMD. Register as an anonymous module.
        define(["jquery"], factory);
    } else if (typeof module === "object" && module.exports) {
        // Node/CommonJS
        module.exports = function (root, jQuery) {
            if (jQuery === undefined) {
                // require("jQuery") returns a factory that requires window to
                // build a jQuery instance, we normalize how we use modules
                // that require this pattern but the window provided is a noop
                // if it"s defined (how jquery works)
                if (typeof window !== "undefined") {
                    jQuery = require("jquery");
                } else {
                    jQuery = require("jquery")(root);
                }
            }
            factory(jQuery);
            return jQuery;
        };
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

    $.fn.api = function () {
        return "";
    };

}));
';

$minified = JCompressor::js($js, true); 

/* // Valor del $minified:
!function(n){"function"==typeof define&&define.amd?define(["jquery"],n):"object"==typeof module&&module.exports?module.exports=function(e,t){return void 0===t&&(t="undefined"!=typeof window?require("jquery"):require("jquery")(e)),n(t),t}:n(jQuery)}(function(n){n.fn.api=function(){return""}});
*/
```

**Ejemplo 4:**

Minificar contenido JS de forma mas completa utilizando el portal Toptal y almacenaje en cache para evitar lentitud en posibles consultas multiples

```php
require_once 'vendor/autoload.php';

$js = '
(function (factory) {
    if (typeof define === "function" && define.amd) {
        // AMD. Register as an anonymous module.
        define(["jquery"], factory);
    } else if (typeof module === "object" && module.exports) {
        // Node/CommonJS
        module.exports = function (root, jQuery) {
            if (jQuery === undefined) {
                // require("jQuery") returns a factory that requires window to
                // build a jQuery instance, we normalize how we use modules
                // that require this pattern but the window provided is a noop
                // if it"s defined (how jquery works)
                if (typeof window !== "undefined") {
                    jQuery = require("jquery");
                } else {
                    jQuery = require("jquery")(root);
                }
            }
            factory(jQuery);
            return jQuery;
        };
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

    $.fn.api = function () {
        return "";
    };

}));
';

## Primera ejecución (enviará el contenido a toptal para comprimirse y guarda el resultado en cache)
$minified = JCompressor::js($js, true, 3600); // time: 0.66719508171082

## segunda ejecución (retornará la cache previamente guardada)
$minified = JCompressor::js($js, true, 3600); // time: 0.062622785568237

## tercera ejecución (retornará la cache previamente guardada y cargada)
$minified = JCompressor::js($js, true, 3600); // time: 0.00041890144348145
```
